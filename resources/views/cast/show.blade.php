@extends('layout.master')
@section('judul')
    Halaman Show Cast    
@endsection
@section('content')
<div>
        <h2>Show Cast {{$cast->id}}</h2>
        <div>
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" name="name" value="{{$cast->name}}" id="name" placeholder="Masukkan Nama">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" name="bio" cols="30" id="bio" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
            </div>
        </div>
</div>
@endsection